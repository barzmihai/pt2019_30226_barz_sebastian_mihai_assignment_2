package ass2;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

public class GUI implements ActionListener {

	JFrame appFrame;
	JLabel jlbMina, jlbMaxa, jlbMins, jlbMaxs, jlbNrq, jlbSimInt; 
	JTextField jtfMina, jtfMaxa, jtfMins, jtfMaxs;
	static JTextField jtfNrq;
	JTextField jtfSimInt;
	JTextField jtfResult;
	JButton jbnStart;
	Container cPane;
	static int ok=0;
	static JScrollPane scroll,scroll2;
	public static JTextArea display,display2;
	
	public GUI(){
		createGUI();
	}
	
	public void createGUI(){

   		/*Create a frame, get its contentpane and set layout*/
   		appFrame = new JFrame("Threads");
   		cPane = appFrame.getContentPane();
   		cPane.setLayout(new GridBagLayout());
  
   		//Arrange components on contentPane and set Action Listeners to each JButton

   		arrangeComponents();
   		appFrame.pack();
   		appFrame.setResizable(false);
   		appFrame.setVisible(true);
   		
   	}	
	
	public void arrangeComponents(){
		
		jlbMina=new JLabel("Minimum arriving time:");
		jlbMaxa=new JLabel("Maximum arriving time:");
		jlbMins=new JLabel("Minimum service time:");
		jlbMaxs=new JLabel("Maximum service time:");
		jlbNrq=new JLabel("Number of queues:");
		jlbSimInt=new JLabel("Simulation interval:");
	
		jtfMina = new JTextField(10);
		jtfMaxa = new JTextField(10);
		jtfMins  = new JTextField(10);
		jtfMaxs = new JTextField(10);
		jtfNrq = new JTextField(10);
		jtfSimInt = new JTextField(10);
		jtfResult = new JTextField(1000);
		
		jbnStart = new JButton("Start");
		
	    display = new JTextArea(15,58);
	    display.setEditable(false); // set textArea non-editable
	    JScrollPane scroll = new JScrollPane(display);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    display.setText("");
	   
	    display2 = new JTextArea(15,58);
	    display2.setEditable(false); // set textArea non-editable
	    JScrollPane scroll2 = new JScrollPane(display2);
	    scroll2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    display2.setText("");
	   
	    GridBagConstraints g20 = new GridBagConstraints();
	    g20.gridwidth = 5;
	    g20.gridx=5;
		g20.gridy=5;
		g20.insets = new Insets(0,0,0,0);
		cPane.add(scroll2,g20);
	    
	    GridBagConstraints g0 = new GridBagConstraints();
	    g0.gridwidth = 5;
	    g0.gridx=0;
		g0.gridy=5;
		g0.insets = new Insets(0,0,0,0);
		cPane.add(scroll,g0);
	   
		GridBagConstraints g1 = new GridBagConstraints();
		g1.gridx=0;
		g1.gridy=0;
		g1.insets = new Insets(2,2,2,2);
		cPane.add(jlbMina,g1);
		
		GridBagConstraints g2 = new GridBagConstraints();
		g2.gridx=0;
		g2.gridy=1;
		g2.insets = new Insets(2,2,2,2);
		cPane.add(jlbMaxa,g2);
		
		GridBagConstraints g3 = new GridBagConstraints();
		g3.gridx=1;
		g3.gridy=0;
		g3.insets = new Insets(2,2,2,2);
		cPane.add(jtfMina,g3);
		
		GridBagConstraints g4 = new GridBagConstraints();
		g4.gridx=1;
		g4.gridy=1;
		g4.insets = new Insets(2,2,2,2);
		cPane.add(jtfMaxa,g4);
		
		GridBagConstraints g5 = new GridBagConstraints();
		g5.gridx=2;
		g5.gridy=0;
		g5.insets = new Insets(2,2,2,2);
		cPane.add(jlbMins,g5);
		
		GridBagConstraints g6 = new GridBagConstraints();
		g6.gridx=2;
		g6.gridy=1;
		g6.insets = new Insets(2,2,2,2);
		cPane.add(jlbMaxs,g6);
		
		GridBagConstraints g7 = new GridBagConstraints();
		g7.gridx=3;
		g7.gridy=0;
		g7.insets = new Insets(2,2,2,2);
		cPane.add(jtfMins,g7);
		
		GridBagConstraints g8 = new GridBagConstraints();
		g8.gridx=3;
		g8.gridy=1;
		g8.insets = new Insets(2,2,2,2);
		cPane.add(jtfMaxs,g8);
		
		GridBagConstraints g9 = new GridBagConstraints();
		g9.gridx=4;
		g9.gridy=0;
		g9.insets = new Insets(2,2,2,2);
		cPane.add(jlbNrq,g9);
		
		GridBagConstraints g10 = new GridBagConstraints();
		g10.gridx=5;
		g10.gridy=0;
		g10.insets = new Insets(2,2,2,2);
		cPane.add(jtfNrq,g10);
		
		GridBagConstraints g11 = new GridBagConstraints();
		g11.gridx=4;
		g11.gridy=1;
		g11.insets = new Insets(2,2,2,2);
		cPane.add(jlbSimInt,g11);
		
		GridBagConstraints g12 = new GridBagConstraints();
		g12.gridx=5;
		g12.gridy=1;
		g12.insets = new Insets(2,2,2,2);
		cPane.add(jtfSimInt,g12);
		
		GridBagConstraints g13 = new GridBagConstraints();
		g13.gridx=5;
		g13.gridy=2;
		g13.insets = new Insets(2,2,2,2);
		cPane.add(jbnStart,g13);
		
		jbnStart.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbnStart){
			s();
		}
	}

	public void s(){
		int i,min=0,max=0,min1=0,max1=0;
		String nr1 =jtfNrq.getText();
		String min2 =jtfMins.getText();
		String max2 =jtfMaxs.getText();
		String min3 =jtfMina.getText();
		String max3 =jtfMaxa.getText();
		int nr = Integer.parseInt(nr1);
		min = Integer.parseInt(min2);
		max = Integer.parseInt(max2);
		min1 = Integer.parseInt(min3);
		max1 = Integer.parseInt(max3);
		String s=jtfSimInt.getText();
		int simInt= Integer.parseInt(s);
		Queue c[] = new Queue[ nr ];
		for( i=0; i<nr; i++){
			c[ i ] = new Queue("Queue "+Integer.toString( i ));
			c[ i ].start();
		} 
		//System.out.println(min);
		//System.out.println(max);
		//System.out.println(min1);
		//System.out.println(max1);
		Shop sh = new Shop( nr , c, "Shop",min1,max1,min,max,simInt);
		sh.start();
	}
	
	public static void main( String args[] ){
		GUI g = new GUI();
	}
}
