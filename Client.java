package ass2;

import java.util.*;

public class Client {

	private int id, service, arrival;
	
	public Client(int id, int arrival, int service){
		this.id=id;
		this.arrival=arrival;
		this.service=service;
	}
	
	public int getID(){
		return id;
	}
	
	public int getArrival(){
		return arrival;
	}
	
	public int getService(){
		return service*1000;
	}
	
	public String toString(){
		return (Integer.toString(id)+" "+Integer.toString(arrival)+" "+Integer.toString(service));
	}
}