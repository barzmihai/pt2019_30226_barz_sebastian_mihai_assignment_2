package ass2;

import java.util.*;

public class Shop extends Thread{
	private Queue queue[];
	private int nr_queues, minService, maxService, minArrival, maxArrival, simInt;
	static int ID =0;
	private long startTime = System.currentTimeMillis();	
	public Shop( int nr_queues, Queue queue[], String name, int minArrival,int maxArrival, int minService,int maxService, int simInt){
		setName( name );
		this.simInt=simInt;
		this.nr_queues = nr_queues;
		this.minService=minService;
		this.maxService=maxService;
		this.minArrival=minArrival;
		this.maxArrival=maxArrival;
		this.queue = new Queue[nr_queues];
		for( int i=0; i<nr_queues; i++){
			this.queue[i] =queue[i] ; 	
		}
	 }
	
	 private int min_index (){
		int index = 0;
		try{
			long min = queue[0].queueSize();
			for( int i=1; i<nr_queues; i++){
			 long lung = queue[ i ].queueSize();
			 if ( lung < min ){
				 min = lung;
				 index = i;
			 }
			}
		 }
		 catch( InterruptedException e ){
			 System.out.println( e.toString());
		 }
		 return index;
	 }

	 
	 public void run(){
		 while( System.currentTimeMillis()-startTime<simInt*1000){
			 try{
				 Random r = new Random();
				 int Result = r.nextInt(maxService-minService)+minService;
				 int Result2 =r.nextInt(maxArrival-minArrival)+minArrival;
				 Client c = new Client( ++ID, Result2, Result );
				 int m = min_index();
				 GUI.display.append("Client:" +Long.toString( ID )+" arrived at "+Result2+" added at queue "+ Integer.toString(m)+ " with service time: "+Result+" sec\n");
				 queue[m].addClient( c );
				 int i=0,j=0;
				 for (i=0;i<nr_queues;i++){
					 for (j=0;j<queue[i].queueSize();j++){
						 GUI.display2.setText("");
						 for (int k=0;k<nr_queues;k++){
						 GUI.display2.append("queue:"+k);
						 GUI.display2.append(" has "+queue[k].queueSize()+" clients\n");
						 }
					 }
				 }
				// System.out.println(System.currentTimeMillis()+"   "+ startTime);
				 sleep( Result2*1000 );
			 }
		 
			 catch( InterruptedException e ){
				 System.out.println( e.toString());
		 	 }
		 }
		 int c=0,s=0;
		
		 for (int i=0; i<queue.length;i++){
			c+=queue[i].getAvg()/1000;
			s+=queue[i].getd();
			//System.out.println("Average service time: "+c+"   "+s);
		 }
		 c=c/s;
		 GUI.display.append("Average service time: "+c+" sec\n");
	}
}