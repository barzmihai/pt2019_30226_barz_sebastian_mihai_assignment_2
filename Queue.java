package ass2;

import java.io.PrintStream;
import java.util.*;
 

public class Queue extends Thread{
private Vector clients=new Vector(); 
	private int avgTime=0;
	private int d=0;
	public Queue( String name ){
		setName( name );
	}
	
	public void run(){
		try{
			while( true ){
				int service=getS();
				avgTime+=service;
				removeClient();
				sleep( service );
			}
		 }
		 catch( InterruptedException e ){
			 System.out.println("Error");
			 System.out.println( e.toString());
		 	}
		 }
	
	
	public synchronized void addClient( Client c ) throws InterruptedException{
		clients.addElement(c);
		notifyAll();
	
	}
	
	public synchronized int getS() throws InterruptedException{
		while( clients.size() == 0 )
			wait();
			Client c = ( Client )clients.elementAt(0);
			notifyAll();
			return c.getService();
	}
	
	public synchronized void removeClient() throws InterruptedException{
		while( clients.size() == 0 )
		wait();
		Client c = ( Client )clients.elementAt(0);
		clients.removeElementAt(0);
		d++;
		GUI.display.append(Long.toString( c.getID())+" finished shopping at queue "+getName()+"\n");
		notifyAll();
	}
	
	public synchronized long queueSize() throws InterruptedException{
		notifyAll();
		long size = clients.size();
		return size;
	}
	
	public int getAvg(){
		return avgTime;
	}
	
	public int getd(){
		return d;
	}
}
